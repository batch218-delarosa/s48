//console.log('test s48')

//Mock database
let posts = [];

// Posts ID
let count = 1;

// ADD POST DATA

document.querySelector('#form-add-post').addEventListener('submit', e => {

    // Prevents the page from reloading
    // prevents default behavior of event
    e.preventDefault();

    posts.push({
        id: count,
        title: document.querySelector('#txt-title').value,
        body: document.querySelector('#txt-body').value
    })

    count++;

    updatePosts()
    //showPosts();

});

// const showPosts = () => {
//     let postEntries = '';

//     posts.forEach(post => {
//         postEntries += 
//         `
//         <div id="post-${post.id}">
//             <h3 id="post=title-${post.id}">${post.title}</h3>
//             <p id="post-body-${post.id}">${post.body}</p>
//             <button onclick="editPost('${post.id}')">Edit</button>
//             <button onclick="deletePost('${post.id}')">Delete</button>

//         </div>   
//         `
//     });

//     document.querySelector('#div-post-entries').innerHTML = postEntries;
// }

// Retrieve Posts

function updatePosts() {

    const postsElement = document.querySelector('#div-post-entries');

    while (postsElement.hasChildNodes()) {
        postsElement.removeChild(postsElement.children[0]);
    }
    
    for (let post of posts) {
        const title = document.createElement('h3');
        const body = document.createElement('p');
        const div = document.createElement('div');

        const editBtn = document.createElement('button');
        editBtn.setAttribute('onclick', `editPost(${post.id})`)
        editBtn.innerHTML = "Edit"

        const deleteBtn = document.createElement('button');
        deleteBtn.setAttribute('onclick', `deletePost(${post.id})`)
        deleteBtn.innerHTML = "Delete"


        div.setAttribute('class', 'post-div');

        title.innerHTML = post.title;
        body.innerHTML = post.body;

        div.appendChild(title);
        div.appendChild(body);
        div.appendChild(editBtn);
        div.appendChild(deleteBtn);

        postsElement.appendChild(div);

    }
}

function deletePost(id) {

    const index = posts.findIndex(post => {
        return post.id == id;
    });

    posts.splice(index, 1);
    updatePosts();


}

function editPost(id) {

    const index = posts.findIndex(post => {
        return post.id == id;
    });

    const postTarget = posts[index];

    const editTitleNode = document.getElementById('txt-edit-title');
    const editBodyNode = document.getElementById('txt-edit-body');
    const editForm = document.getElementById('form-edit-post');

    editTitleNode.value = postTarget.title;
    editBodyNode.value = postTarget.body;

    editForm.hidden = false;


    editForm.onsubmit = (e) => {
        e.preventDefault();
        const newTitle = editTitleNode.value;
        const newBody = editBodyNode.value;

        // postTarget created a reference to posts[index] object. BUT when we use the postTarget variable as an object, when we repalce it's properties the properties of the original object in the array is modified. because it's an object? and objects behave differently then other data types?

        postTarget.title = newTitle;
        postTarget.body = newBody;

        // Nice no need for splice #objectswow

        // Reset and hide edit post form
        editTitleNode.value = '';
        editBodyNode.value = '';
        editForm.hidden = true;

        updatePosts();

        // Since we're using anonymous functions, it's difficult to removeEventListeners (or impossible? not sure) so we resort to using the onclick method so we can easily replace the onsubmit function.
        editForm.onsubmit = (e) => {
            e.preventDefault();
        };

    }


}

document.getElementById('form-edit-post').onsubmit = (e) => {
    e.preventDefault();
}